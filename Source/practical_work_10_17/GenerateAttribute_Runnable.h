// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MessageEndpoint.h"
#include "HAL/Runnable.h"

class Apractical_work_10_17GameModeBase;

class PRACTICAL_WORK_10_17_API UGenerateAttribute_Runnable : public FRunnable
{

public:
	UGenerateAttribute_Runnable(Apractical_work_10_17GameModeBase* OwnerClass);

	Apractical_work_10_17GameModeBase* GameMode_Ref;

	virtual bool Init() override;
	virtual uint32 Run() override;
	virtual void Stop() override;
	virtual void Exit() override;

	int8_t GetRandom();

	TSharedPtr<FMessageEndpoint, ESPMode::ThreadSafe> SenderEndpoint;
};
