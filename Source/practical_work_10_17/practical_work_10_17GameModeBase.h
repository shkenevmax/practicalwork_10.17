// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MessageEndpoint.h"
#include "IMessageBus.h"
#include "GenerateAttribute_Runnable.h"
#include "practical_work_10_17GameModeBase.generated.h"

USTRUCT(BlueprintType)
struct FBusStructMessage_AttributeGenerator
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadWrite)
	int CubeAge = 0;
	UPROPERTY(BlueprintReadWrite)
	FColor CubeColor = FColor();
	FBusStructMessage_AttributeGenerator(int8_t newCubeAge = 0, FColor newCubeColor = FColor()) : CubeAge(newCubeAge), CubeColor(newCubeColor) {}
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnUpdateByAttributeGeneratorThread, FBusStructMessage_AttributeGenerator, AttributeValue);

UCLASS()
class PRACTICAL_WORK_10_17_API Apractical_work_10_17GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
public:
	void BeginPlay() override;

	class UGenerateAttribute_Runnable* GenerateAttribute_RunnableThread = nullptr;
	FRunnableThread* CurrentRunningGMThread_GenerateAttribute = nullptr;

	UPROPERTY(BlueprintAssignable)
	FOnUpdateByAttributeGeneratorThread OnUpdateByAgeGeneratorThread;

	TSharedPtr<FMessageEndpoint, ESPMode::ThreadSafe> ReceiveEndpoint_AttributeGenerator;

	void EventMessage_AttributeGenerator(FBusStructMessage_AttributeGenerator Attribute);

	void BusMessageHandler_AgeGenerator(const struct FBusStructMessage_AttributeGenerator& MessageCubeAttribute, 
		const TSharedRef<IMessageContext, ESPMode::ThreadSafe>& Context);

	int32 CudeCount = 0;
	float sizeY = -700.0f;

	UPROPERTY(EditAnywhere, Category = "Cube settings")
	TSubclassOf<class AOldColoredCube> SpawnObject;
};
