// Copyright Epic Games, Inc. All Rights Reserved.


#include "practical_work_10_17GameModeBase.h"
#include "MessageEndpointBuilder.h"
#include "OldColoredCube.h"
#include "MessageEndpoint.h"

void Apractical_work_10_17GameModeBase::BeginPlay()
{
	Super::BeginPlay();

	if (!CurrentRunningGMThread_GenerateAttribute)
	{
		if (!GenerateAttribute_RunnableThread) GenerateAttribute_RunnableThread = new UGenerateAttribute_Runnable(this);

		CurrentRunningGMThread_GenerateAttribute = FRunnableThread::Create(GenerateAttribute_RunnableThread, TEXT("Generate attribute thread"), 0, EThreadPriority::TPri_Normal);
	}

	ReceiveEndpoint_AttributeGenerator = FMessageEndpoint::Builder("Receiver_AttributeGenerator").Handling<FBusStructMessage_AttributeGenerator>(this, 
		&Apractical_work_10_17GameModeBase::BusMessageHandler_AgeGenerator);

	if (ReceiveEndpoint_AttributeGenerator.IsValid())
		ReceiveEndpoint_AttributeGenerator->Subscribe<FBusStructMessage_AttributeGenerator>();
}

void Apractical_work_10_17GameModeBase::EventMessage_AttributeGenerator(FBusStructMessage_AttributeGenerator Attribute)
{
	OnUpdateByAgeGeneratorThread.Broadcast(Attribute);
	UWorld* myWorld = GetWorld();
	if (myWorld && SpawnObject)
	{
		FVector SpawnLoc = FVector(-200.0f, sizeY, 300.0f);
		sizeY += 200.0f;
		FRotator SpawnRot;
		AOldColoredCube* OldColoredCube = Cast<AOldColoredCube>(myWorld->SpawnActor(SpawnObject, &SpawnLoc, &SpawnRot, FActorSpawnParameters()));
		if (OldColoredCube)
		{
			OldColoredCube->Init(Attribute);
		}
	}
}

void Apractical_work_10_17GameModeBase::BusMessageHandler_AgeGenerator(const FBusStructMessage_AttributeGenerator& MessageCubeAttribute,
	const TSharedRef<IMessageContext, ESPMode::ThreadSafe>& Context)
{
	EventMessage_AttributeGenerator(MessageCubeAttribute);
}