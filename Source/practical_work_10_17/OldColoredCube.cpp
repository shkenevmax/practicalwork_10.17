// Fill out your copyright notice in the Description page of Project Settings.


#include "OldColoredCube.h"

// Sets default values
AOldColoredCube::AOldColoredCube()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AOldColoredCube::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AOldColoredCube::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AOldColoredCube::Init(FBusStructMessage_AttributeGenerator InitAttributes)
{
	InitBP(InitAttributes);
}

void AOldColoredCube::InitBP_Implementation(FBusStructMessage_AttributeGenerator InitAttributes)
{
	// BP
}

