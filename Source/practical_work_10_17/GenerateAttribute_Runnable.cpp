// Fill out your copyright notice in the Description page of Project Settings.


#include "GenerateAttribute_Runnable.h"
#include "practical_work_10_17GameModeBase.h"
#include "MessageEndpointBuilder.h"
#include "MessageEndpoint.h"
#include <random>

UGenerateAttribute_Runnable::UGenerateAttribute_Runnable(Apractical_work_10_17GameModeBase* OwnerClass)
{
	GameMode_Ref = OwnerClass;

	SenderEndpoint = FMessageEndpoint::Builder("Sender_AttributeGenerator").Build();
}

bool UGenerateAttribute_Runnable::Init()
{
	return true;
}

uint32 UGenerateAttribute_Runnable::Run()
{
	int8 i = 0;

	while (i < 10)
	{
		int8_t GeneratedCubeAge = GetRandom();
		if (GeneratedCubeAge < 0) GeneratedCubeAge *= -1;
		FColor GeneratedCubeColor = FColor(GetRandom(), GetRandom(), GetRandom());

		if (SenderEndpoint.IsValid())
			SenderEndpoint->Publish<FBusStructMessage_AttributeGenerator>(new FBusStructMessage_AttributeGenerator(GeneratedCubeAge, GeneratedCubeColor));

		i++;
	}
	
	return 1;
}

void UGenerateAttribute_Runnable::Stop()
{

}

void UGenerateAttribute_Runnable::Exit()
{

}

int8_t UGenerateAttribute_Runnable::GetRandom()
{
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<> disrib(0, 255);

	return disrib(gen);
}
