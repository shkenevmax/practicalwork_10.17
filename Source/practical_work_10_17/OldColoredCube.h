// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "practical_work_10_17GameModeBase.h"
#include "OldColoredCube.generated.h"

UCLASS(BlueprintType)
class PRACTICAL_WORK_10_17_API AOldColoredCube : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AOldColoredCube();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void Init(FBusStructMessage_AttributeGenerator InitAttributes);

	UFUNCTION(BlueprintNativeEvent)
	void InitBP(FBusStructMessage_AttributeGenerator InitAttributes);
};
